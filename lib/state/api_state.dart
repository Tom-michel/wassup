import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as htpp;
import 'package:localstorage/localstorage.dart';

class ApiState with ChangeNotifier {
  LocalStorage storage = LocalStorage("usertoken");

// login service
  Future<bool> loginNow(String phoneNumer, String password) async {
    try {
      var url = Uri.parse("http://michelbtompe.pythonanywhere.com/login/");
      var response = await htpp.post(
        url,
        headers: {
          "Content-Type": "application/json; charset=UTF-8",
        },
        body: json.encode(
          {
            'username': phoneNumer,
            'password': password,
          },
        ),
      );
      var data = json.decode(response.body) as Map;
      // print(data);
      if (data.containsKey('token')) {
        print("Login succesfully");
        // print(data['token']);
        storage.setItem('token', data['token']);
        // print(storage.getItem('token'));
        return false;
      }
      return true;
    } catch (e) {
      print("Error to login");
      print(e);
      return true;
    }
  }

// register service
  Future<bool> registerNow(
      String phoneNumer, String email, String username, String password) async {
    try {
      var url = Uri.parse("http://michelbtompe.pythonanywhere.com/register/");
      var response = await htpp.post(
        url,
        headers: {
          "Content-Type": "application/json; charset=UTF-8",
        },
        body: json.encode({
          'username': phoneNumer,
          'email': email,
          'last_name': username,
          'password': password,
        }),
      );
      var data = json.decode(response.body) as Map;
      if (data['error'] == false) {
        print("Register succesfully");
      }
      return data['error'];
    } catch (e) {
      print("Error to register");
      print(e);
      return true;
    }
  }
}
