import 'package:flutter/material.dart';
import 'package:wassup/model/ChatModel.dart';
import 'package:wassup/screens/create_group.dart';
import 'package:wassup/screens/select_contact.dart';
import 'package:wassup/widgets/add_bouton.dart';
import 'package:wassup/widgets/customUI/custom_card.dart';

class GroupPage extends StatefulWidget {
  const GroupPage({Key? key}) : super(key: key);

  @override
  _GroupPageState createState() => _GroupPageState();
}

class _GroupPageState extends State<GroupPage> {
  List<ChatModel> chats = [
    ChatModel(
        name: "INf417",
        icon: "groups.svg",
        time: "4:00",
        isGroup: true,
        currentMessage: "We have course today",
        status: "hey i'm using wassup"),
    ChatModel(
        name: "Adam's",
        icon: "assets/images/person.svg",
        time: "6:00",
        isGroup: false,
        currentMessage: "How bro",
        status: "hey i'm using wassup"),
    ChatModel(
        name: "Genie Logiciel SI",
        icon: "groups.svg",
        time: "4:00",
        isGroup: true,
        currentMessage: "We have course today",
        status: "hey i'm using wassup"),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        floatingActionButton: AddButton(
          onPressed: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (builder) => CreateGroup()));
          },
        ),
        body: ListView.builder(
          itemCount: chats.length,
          itemBuilder: (context, index) => CustomCard(chatModel: chats[index]),

          /*SingleChildScrollView(
          child: Column(
        children: [
          CustomCard(),
          CustomCard(),
          CustomCard(),
        ],
      )),*/
        ));
  }
}
