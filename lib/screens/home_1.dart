import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:localstorage/localstorage.dart';
import 'package:wassup/config/config.dart';
import 'package:wassup/screens/home_screen/camera.dart';
import 'package:wassup/screens/home_screen/chat_view.dart';
import 'package:wassup/screens/home_screen/group_view.dart';
import 'package:wassup/screens/home_screen/phone_view.dart';
import 'package:wassup/screens/home_screen/status_view.dart';
import 'package:wassup/screens/signIn.dart';

class Conversation extends StatefulWidget {
  const Conversation({Key? key}) : super(key: key);
  static const rooteName = "conversation-sreens";
  @override
  _ConversationState createState() => _ConversationState();
}

class _ConversationState extends State<Conversation> {
  int currentIndex = 2;
  final screens = [
    CameraPage(),
    PhonePage(),
    ChatPage(),
    GroupPage(),
    StatusPage(),
  ];

  int change() {
    return 1;
  }

  @override
  Widget build(BuildContext context) {
    LocalStorage storage = LocalStorage("usertoken");
    _logoutNow() {
      storage.clear();
      print("Logout successfully");
      Navigator.of(context).pushReplacementNamed(SignIn.rooteName);
    }

    return OverflowBox(
      child: Scaffold(
          appBar: AppBar(
            automaticallyImplyLeading: false,
            backgroundColor: Configu.colors.primaryColor,
            title: Text("Wassup"),
            centerTitle: true,
            actions: [
              IconButton(onPressed: () {}, icon: Icon(Icons.search)),
              IconButton(
                onPressed: () {
                  _logoutNow();
                },
                icon: Icon(Icons.logout_rounded),
              ),
            ],
            leading: InkWell(
              onTap: () {},
              child: Padding(
                padding: const EdgeInsets.only(left: 10),
                child: CircleAvatar(
                  radius: 20,
                  child: SvgPicture.asset(
                    "assets/images/person.svg",
                    color: Colors.white,
                    height: 36,
                    width: 36,
                  ),
                  backgroundColor: Colors.blueGrey,
                ),
              ),
            ),
          ),
          body: screens[currentIndex],
          bottomNavigationBar: BottomNavigationBar(
            backgroundColor: Configu.colors.msgColor2,
            selectedItemColor: Configu.colors.primaryColor,
            type: BottomNavigationBarType.fixed,
            iconSize: 25,
            //selectedFontSize: 10,
            showUnselectedLabels: true,
            showSelectedLabels: true,
            //unselectedFontSize: 20,
            currentIndex: currentIndex,
            onTap: (index) => setState(() => currentIndex = index),
            items: [
              BottomNavigationBarItem(
                  icon: Icon(Icons.camera_alt), label: 'Camera'),
              BottomNavigationBarItem(
                  icon: Icon(Icons.call_end_outlined), label: 'Calls'),
              BottomNavigationBarItem(
                  icon: Icon(Icons.textsms_outlined), label: 'Chats'),
              BottomNavigationBarItem(
                  icon: Icon(Icons.person_outlined), label: 'Groups'),
              BottomNavigationBarItem(
                  icon: Icon(Icons.token_outlined), label: 'Status'),
            ],
          )),
    );
  }
}
