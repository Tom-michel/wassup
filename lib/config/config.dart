import 'package:flutter/painting.dart';

class Configu {
  static final colors = _Color();
  static final assets = _Asset();
}

class _Color {
  final primaryColor = Color(0xFF1FB141);
  final secondColor = Color(0xFF323232);
  final msgColor1 = Color(0xFF2865DC);
  final titleBar = Color(0xFFE9E9E9);
  final msgColor2 = Color(0xFFFFFFFF);
  final textColor = Color(0xFF000000);
}

class _Asset {
  final logo = "assets/images/logo1.png";
  final logo2 = "assets/images/logo2.png";
  final bg = "assets/images/caht_bg.png";
}
