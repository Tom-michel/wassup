import 'package:flutter/material.dart';
import 'package:wassup/config/config.dart';

class Button extends StatelessWidget {
  VoidCallback onPressed;
  Color? buttonColor;
  double? borderRadius;
  Color? textColor;
  String buttonText;
  Color? borderColor;

  Button({
    required this.onPressed,
    this.borderRadius,
    this.borderColor,
    this.buttonColor,
    this.textColor,
    required this.buttonText,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: buttonColor ?? Configu.colors.primaryColor,
          borderRadius: BorderRadius.circular(borderRadius ?? 0)),
      child: Material(
          color: Colors.transparent,
          child: InkWell(
            onTap: onPressed,
            child: Container(
              alignment: Alignment.center,
              height: 55,
              decoration: BoxDecoration(
                  color: Configu.colors.primaryColor,
                  borderRadius: BorderRadius.circular(6)),
              child: Text(
                buttonText,
                style: TextStyle(
                    color: textColor,
                    fontWeight: FontWeight.w500,
                    fontSize: 18),
              ),
            ),
          )),
    );
  }
}
