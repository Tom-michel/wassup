import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:wassup/model/ChatModel.dart';
import 'package:wassup/screens/individual_chat.dart';

class ContactCard extends StatelessWidget {
  const ContactCard({
    Key? key,
    required this.contact,
  }) : super(key: key);

  final ChatModel contact;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => IndividualPage(
                      chatModel: this.contact,
                    )));
      },
      child: Column(
        children: [
          ListTile(
            leading: avatar(),
            title: Text(
              contact.name,
              style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.bold,
              ),
            ),
            subtitle: Text(
              contact.status,
              style: TextStyle(
                fontSize: 13,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget avatar() {
    return CircleAvatar(
      radius: 20,
      child: SvgPicture.asset(
        "assets/images/person.svg",
        color: Colors.white,
        height: 20,
        width: 20,
      ),
      backgroundColor: Colors.blueGrey,
    );
  }
}
