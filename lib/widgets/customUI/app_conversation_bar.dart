import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:wassup/config/config.dart';
import 'package:wassup/model/ChatModel.dart';

class AppBarGlobal extends StatefulWidget {
  const AppBarGlobal({Key? key, required this.chatModel}) : super(key: key);
  final ChatModel chatModel;

  @override
  _AppBarGlobalState createState() => _AppBarGlobalState();
}

class _AppBarGlobalState extends State<AppBarGlobal> {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: Configu.colors.msgColor2,
      leadingWidth: 70,
      leading: InkWell(
        onTap: () {
          Navigator.pop(context);
        },
        child: Padding(
          padding: const EdgeInsets.only(left: 9),
          child: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
            Icon(
              Icons.arrow_back,
              size: 20,
              color: Colors.black,
            ),
            Spacer(flex: 8),
            CircleAvatar(
              radius: 20,
              child: SvgPicture.asset(
                widget.chatModel.isGroup
                    ? "assets/images/groups.svg"
                    : "assets/images/person.svg",
                color: Colors.white,
                height: 36,
                width: 36,
              ),
              backgroundColor: Colors.blueGrey,
            ),
          ]),
        ),
      ),
      title: Container(
        margin: EdgeInsets.all(5),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                widget.chatModel.name,
                style: TextStyle(
                  fontSize: 18.5,
                  fontWeight: FontWeight.bold,
                  color: Configu.colors.textColor,
                ),
              ),
              Text(
                "last seen today at 12:05",
                style: TextStyle(
                  fontSize: 13,
                  color: Configu.colors.primaryColor,
                ),
              )
            ]),
      ),
      actions: [
        IconButton(
          onPressed: () {},
          icon: Icon(
            Icons.search,
            size: 30,
          ),
          color: Configu.colors.textColor,
        )
      ],
      centerTitle: true,
    );
  }
}
