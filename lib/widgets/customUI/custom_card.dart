import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:wassup/model/ChatModel.dart';
import 'package:wassup/screens/individual_chat.dart';

class CustomCard extends StatelessWidget {
  final ChatModel chatModel;

  const CustomCard({
    required this.chatModel,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        InkWell(
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) =>
                        IndividualPage(chatModel: this.chatModel)));
          },
          child: ListTile(
            leading: avatar(),
            title: Text(
              chatModel.name,
              style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.bold,
              ),
            ),
            subtitle: Row(children: [
              Icon(Icons.done_all),
              SizedBox(
                width: 4,
              ),
              Text(
                chatModel.currentMessage,
                style: TextStyle(
                  fontSize: 13,
                ),
              )
            ]),
            trailing: Text(chatModel.time),
          ),
        ),
        const Padding(
          padding: EdgeInsets.zero,
          child: Divider(
            thickness: 0.25,
          ),
        ),
      ],
    );
  }

  Widget avatar() {
    return CircleAvatar(
      radius: 30,
      child: SvgPicture.asset(
        chatModel.isGroup
            ? "assets/images/groups.svg"
            : "assets/images/person.svg",
        color: Colors.white,
        height: 37,
        width: 37,
      ),
      backgroundColor: Colors.blueGrey,
    );
  }
}
